const Sequelize = require('sequelize');

module.exports = {
  /**
   * @param {Sequelize} Sequelize
   * @param {QueryInterface} queryInterface
   */
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('users', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      },
      investor_name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      city: {
        type: Sequelize.STRING,
        allowNull: false
      },
      country: {
        type: Sequelize.STRING,
        allowNull: false
      },
      balance: {
        type: Sequelize.DECIMAL,
        allowNull: false
      },
      photo: {
        type: Sequelize.STRING,
        allowNull: false
      },
      portfolio_value: {
        type: Sequelize.DECIMAL,
        allowNull: false
      },
      first_access: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: false
      },
      super_angel: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: false
      },
      portfolio_id: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      password_hash: {
        type: Sequelize.STRING,
        allowNull: false
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false
      }
    });
  },

  down: queryInterface => {
    return queryInterface.dropTable('users');
  }
};
