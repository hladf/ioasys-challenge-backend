import { getEnv } from '../utils/env';

export default {
  secret: getEnv('JWT_SECRET'),
  expiresIn: getEnv('JWT_EXPIRESIN')
};
