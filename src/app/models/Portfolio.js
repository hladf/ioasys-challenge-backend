import Sequelize, { Model } from 'sequelize';

class Portfolio extends Model {
  static init(sequelize) {
    super.init(
      {
        enterprises_number: {
          type: Sequelize.VIRTUAL,
          get() {
            return 0;
          }
        },
        enterprises: {
          type: Sequelize.VIRTUAL,
          get() {
            return [];
          }
        }
      },
      {
        sequelize
      }
    );

    return this;
  }

  // static associate(models) {
  //   this.belongsTo(models.User, {
  //     foreignKey: 'portfolio_id',
  //     as: 'portfolio'
  //   });
  // }
}

export default Portfolio;
