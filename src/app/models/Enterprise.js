import Sequelize, { Model } from 'sequelize';

class Enterprise extends Model {
  static init(sequelize) {
    super.init(
      {
        investor_name: Sequelize.STRING,
        balance: Sequelize.NUMBER,
        enterprise_name: Sequelize.STRING,
        description: Sequelize.STRING,
        email_enterprise: Sequelize.STRING,
        facebook: Sequelize.STRING,
        twitter: Sequelize.STRING,
        linkedin: Sequelize.STRING,
        phone: Sequelize.STRING,
        own_enterprise: Sequelize.BOOLEAN,
        photo: Sequelize.STRING,
        value: Sequelize.NUMBER,
        shares: Sequelize.NUMBER,
        share_price: Sequelize.NUMBER,
        own_shares: Sequelize.NUMBER,
        city: Sequelize.STRING,
        country: Sequelize.STRING,
        enterprise_type: {
          type: Sequelize.STRING,
          get() {
            return {
              id: 21,
              enterprise_type_name: 'Software'
            };
          }
        }
      },
      {
        sequelize
      }
    );

    return this;
  }
}

export default Enterprise;
