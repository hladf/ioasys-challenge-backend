import { Router } from 'express';

// import EnterpriseRouter from './Enterprises';
import UserRouter from './Users';

const router = Router();

router.use('/users', UserRouter);

// router.use('/enterprise', EnterpriseRouter);

export default router;
