import { Router } from 'express';
import authMiddleware from '../../../app/midlewares/auth';
import AuthValidator from '../../validators/AuthValidator';

import { auth } from './auth';

const router = Router();

router.post('/auth/sign_in', AuthValidator, auth);

// begin to authenticate after auth route
router.use(authMiddleware);

export default router;
