import { Response, Request } from 'express';

import jwt from 'jsonwebtoken';

import User from '../../models/User';
// import Portfolio from '../../models/Portfolio';

import authConfig from '../../../config/auth';

/**
 *
 * @param {Request} req
 * @param {Response} res
 */
export async function auth(req, res) {
  const { email, password } = req.body;

  const user = await User.findOne({
    where: { email }
    // ,include: [
    //   {
    //     model: Portfolio,
    //     as: 'portfolio'
    //   }
    // ]
  });

  console.log({ user });

  if (!user) {
    return res.status(401).json({ error: 'User not found' });
  }

  if (!(await user.checkPassword(password))) {
    return res.status(401).json({ error: 'Password does not match' });
  }

  const { id } = user;
  return res.json({
    investor: user,
    token: jwt.sign({ id }, authConfig.secret, {
      expiresIn: authConfig.expiresIn
    }),
    enterprise: null,
    success: true
  });
}
