/**
 * Get an enviroment variable and if fails, returns null or defaultValue
 * @param {string} envVar eviroment var name
 * @param {string|number} defaultValue default value to return if envVar is null
 */
export function getEnv(envVar, defaultValue = null) {
  return process.env[envVar] || defaultValue;
}
