import app from './app';
import { getEnv } from './utils/env';

app.listen(getEnv('APP_PORT', 3333));

console.log('\n>>> Server ON <<<\n');
