import { Router } from 'express';

import mainRouter from './app/routes';

const router = Router();

router.use('/api/v1', mainRouter);

export default router;
